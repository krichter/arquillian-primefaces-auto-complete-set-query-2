package richtercloud.project1.it;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.drone.api.annotation.Drone;
import static org.jboss.arquillian.graphene.Graphene.guardAjax;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omnifaces.utils.arquillian.ArquillianPrimeFaces;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

@RunWith(Arquillian.class)
@RunAsClient
public class TheIT {
    private final static Logger LOGGER = LoggerFactory.getLogger(TheIT.class);

    @Deployment(testable = false)
    public static Archive<?> createDeployment0() throws TransformerException, XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        WebArchive retValue = Maven.configureResolver().workOffline().resolve("richtercloud:arquillian-primefaces-auto-complete-set-query-2-web:war:1.0-SNAPSHOT")
                .withoutTransitivity().asSingle(WebArchive.class);

        ByteArrayOutputStream archiveContentOutputStream = new ByteArrayOutputStream();
        retValue.writeTo(archiveContentOutputStream, Formatters.VERBOSE);
        LOGGER.info(archiveContentOutputStream.toString());
        return retValue;
    }

    @Drone
    private WebDriver browser;
    @ArquillianResource
    private URL deploymentUrl;
    @FindBy(id="mainForm:openDialogButton")
    private WebElement openDialogButton;
    @FindBy(id="mainForm:creationModeSelectButton")
    private WebElement creationModeSelectButton;
    @FindBy(id="mainForm:autoComplete")
    private WebElement autoComplete;
    @Test
    public void testAll() throws IOException {
        browser.get(generateURL("index.xhtml"));
        guardAjax(openDialogButton).click();
        new WebDriverWait(browser, 5).until(ExpectedConditions.visibilityOf(creationModeSelectButton));
        ArquillianPrimeFaces.setSelectOneButtonValue(creationModeSelectButton,
                "MODE1");
            //@TODO: share constants
        final String inexistingOfferTypeName = "abc def 123 xzy";
        new WebDriverWait(browser, 5).until(ExpectedConditions.visibilityOf(autoComplete));
        ArquillianPrimeFaces.configureTimeouts(browser, Duration.ofSeconds(30));
        ArquillianPrimeFaces.setAutoCompleteQuery(autoComplete,
                inexistingOfferTypeName);
    }

    private String generateURL(String page) {
        String retValue = String.format("%s%s",
                deploymentUrl.toExternalForm(),
                page);
        return retValue;
    }
}
