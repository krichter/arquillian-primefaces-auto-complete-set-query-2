package richtercloud.project1.web;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import richtercloud.project1.jar.entities.Entity1;

@Named
@SessionScoped
public class Entity1Session implements Serializable {
    private static final long serialVersionUID = 1L;
    private Entity1 lastEntity1;

    public Entity1Session() {
    }

    public Entity1 getLastEntity1() {
        return lastEntity1;
    }

    public void setLastEntity1(Entity1 lastEntity1) {
        this.lastEntity1 = lastEntity1;
    }
}
