package richtercloud.project1.web;

import java.io.Serializable;

public enum ModeEnum implements Serializable {
    MODE0("mode0"), MODE1("mode1");

    private final String representation;

    ModeEnum(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
