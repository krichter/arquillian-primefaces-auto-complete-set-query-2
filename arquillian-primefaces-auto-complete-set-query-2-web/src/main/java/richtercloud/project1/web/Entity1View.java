package richtercloud.project1.web;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import richtercloud.project1.jar.entities.Entity0;
import richtercloud.project1.jar.entities.Entity1;

@Named
@ViewScoped
public class Entity1View implements Serializable {
    private static final long serialVersionUID = 1L;
    private Entity1 selectedEntity1;
    private Entity0 edit = new Entity0();
    @Inject
    private Entity1Session entity1Session;
    private ModeEnum creationMode = ModeEnum.MODE0;

    public Entity1View() {
    }

    public Entity1 getSelectedEntity1() {
        return selectedEntity1;
    }

    public void setSelectedEntity1(Entity1 selectedEntity1) {
        this.selectedEntity1 = selectedEntity1;
    }

    public List<Entity1> completeSearchText(String input) {
        return new LinkedList<>();
    }

    public ModeEnum getCreationMode() {
        return creationMode;
    }

    public void setCreationMode(ModeEnum creationMode) {
        this.creationMode = creationMode;
    }

    public ModeEnum[] getCreationModes() {
        return ModeEnum.values();
    }

    public Entity0 getEdit() {
        return edit;
    }

    public void setEdit(Entity0 edit) {
        this.edit = edit;
    }

    public boolean displayAdvancedModeComponents() {
        boolean retValue = creationMode == ModeEnum.MODE1;
        return retValue;
    }

    public void onCreationModeChange(AjaxBehaviorEvent event) {
        switch(this.creationMode) {
            case MODE0:
                this.entity1Session.setLastEntity1(this.edit.getEntity1());
                this.edit.setEntity1(null);
                break;
            case MODE1:
                this.edit.setEntity1(this.entity1Session.getLastEntity1());
                break;
            default:
                throw new IllegalStateException(String.format("offer creation "
                        + "mode %s not supported",
                        creationMode));
        }
    }
}
