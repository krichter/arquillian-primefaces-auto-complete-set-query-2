package richtercloud.project1.jar.entities;

public class Entity0 extends Identifiable {
    private static final long serialVersionUID = 1L;
    private Entity1 entity1;

    public Entity0() {
    }

    public Entity0(Entity1 entity1) {
        this.entity1 = entity1;
    }

    public Entity1 getEntity1() {
        return entity1;
    }

    public void setEntity1(Entity1 entity1) {
        this.entity1 = entity1;
    }
}
