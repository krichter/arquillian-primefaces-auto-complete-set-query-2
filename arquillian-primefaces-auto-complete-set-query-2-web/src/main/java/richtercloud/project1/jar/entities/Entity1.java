package richtercloud.project1.jar.entities;

public class Entity1 extends Identifiable {
    private static final long serialVersionUID = 1L;
    private String name;

    public Entity1() {
    }

    public Entity1(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
